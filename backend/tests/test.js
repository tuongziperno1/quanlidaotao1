// tests/ctdt.test.js
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index.js'); // Thay đổi đường dẫn đến tệp chính của ứng dụng của bạn
const expect = chai.expect;

chai.use(chaiHttp);

describe('Test Chương trình đào tạo API', function () {
  this.timeout(20000); // Đặt timeout của bài kiểm tra này là 5000ms (5 giây)
  
  it('nên trả về danh sách chương trình đào tạo', function (done) {
    chai.request('http://localhost:4000')
      .get('/api/ctdt')
      .end(function (err, res) {
        expect(res).to.have.status(200);
        expect(res.body).to.be.an('array');        
        done();
        process.exit(0); // Thoát chương trình với mã lỗi 0 (không có lỗi)
      });
  });
});
